/*
 * Copyright 2000-2017 Vaadin Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package lk.trainingpartner.git.training.ui.views.developerslist;

import com.vaadin.flow.component.Key;
import com.vaadin.flow.component.KeyModifier;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.Grid.SelectionMode;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.H2;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.notification.Notification.Position;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.renderer.ComponentRenderer;
import com.vaadin.flow.data.value.ValueChangeMode;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import lk.trainingpartner.git.training.backend.Category;
import lk.trainingpartner.git.training.backend.CategoryService;
import lk.trainingpartner.git.training.backend.Review;
import lk.trainingpartner.git.training.backend.ReviewService;
import lk.trainingpartner.git.training.ui.MainLayout;
import lk.trainingpartner.git.training.ui.common.AbstractEditorDialog;
import lk.trainingpartner.git.training.ui.views.categorieslist.CategoryEditorDialog;

import java.util.ArrayList;
import java.util.List;

/**
 * Displays the list of available categories, with a search filter as well as
 * buttons to add a new category or edit existing ones.
 */
@Route(value = "developers", layout = MainLayout.class)
@PageTitle("Developers List")
public class DevelopersList extends VerticalLayout {

    private final H2 header = new H2("Developer List");
    private final Grid<String> grid = new Grid<>();

    public DevelopersList() {
        initView();
        addContent();
        updateView();
    }

    private void initView() {
        addClassName("categories-list");
        setDefaultHorizontalComponentAlignment(Alignment.STRETCH);
    }


    private void addContent() {
        VerticalLayout container = new VerticalLayout();
        container.setClassName("view-container");
        container.setAlignItems(Alignment.STRETCH);

        grid.addColumn(String::new).setHeader("Developer").setWidth("8em")
                .setResizable(true);
        grid.setSelectionMode(SelectionMode.NONE);

        container.add(header, grid);
        add(container);
    }

    private void updateView() {
        List<String> categories = new ArrayList<>();
        categories.add(0, "Sampaths W");
        categories.add(1, "");
        categories.add(2, "");
        categories.add(3, "");
        categories.add(4, "");
        categories.add(5, "");
        categories.add(6, "");
        categories.add(7, "");
        categories.add(8, "");
        categories.add(9, "");
        categories.add(10, "");
        grid.setItems(categories);
    }
}
